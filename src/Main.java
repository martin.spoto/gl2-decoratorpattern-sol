import decorator.Personnage;
import decorator.decorators.Affichable;
import decorator.decorators.Bouclier;
import decorator.decorators.Deplacable;
import decorator.decorators.Epee;
import decorator.personnages.Ennemi;
import decorator.personnages.Joueur;

public class Main {

   public static void main(String[] args) {
      Personnage joueurSimple = new Joueur("Joueur 1");
      System.out.println(joueurSimple);

      Personnage joueurEpeeBouclier = new Joueur("Joueur 2");
      joueurEpeeBouclier = new Epee(joueurEpeeBouclier);
      joueurEpeeBouclier = new Bouclier(joueurEpeeBouclier);
      System.out.println(joueurEpeeBouclier);

      Personnage joueurDeuxEpees = new Joueur("Joueur 3");
      joueurDeuxEpees = new Epee(joueurDeuxEpees);
      joueurDeuxEpees = new Epee(joueurDeuxEpees);
      System.out.println(joueurDeuxEpees);

      Personnage ennemiBouclier  = new Ennemi();
      ennemiBouclier = new Bouclier(ennemiBouclier);
      System.out.println(ennemiBouclier);

      System.out.println();

      joueurDeuxEpees.action();
      System.out.println();

      joueurDeuxEpees = new Affichable(joueurDeuxEpees);
      joueurDeuxEpees.action();
      System.out.println();

      joueurDeuxEpees = new Deplacable(joueurDeuxEpees);
      joueurDeuxEpees.action();
      System.out.println();
   }
}
