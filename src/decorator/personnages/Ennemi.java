package decorator.personnages;

import decorator.Personnage;

public class Ennemi extends Personnage {
   public Ennemi() {
      setNom("Ennemi");
      setAttaque(10);
      setDefense(10);
   }
}
