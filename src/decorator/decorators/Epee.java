package decorator.decorators;

import decorator.DecoracteurPersonnage;
import decorator.Personnage;

public class Epee extends DecoracteurPersonnage {

   public Epee(Personnage p) {
      super(p);
   }

   @Override
   public String getNom() {
      return personnage.getNom() + " (+épée)";
   }

   @Override
   public int getAttaque() {
      return personnage.getAttaque() + 20;
   }
}
