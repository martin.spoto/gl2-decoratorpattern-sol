package decorator.decorators;

import decorator.DecoracteurPersonnage;
import decorator.Personnage;

public class Deplacable extends DecoracteurPersonnage {
   public Deplacable(Personnage p) {
      super(p);
   }

   @Override
   public void action() {
      personnage.action();
      System.out.println("Déplace le personnage selon les touches du clavier...");
   }
}
