package decorator.decorators;

import decorator.DecoracteurPersonnage;
import decorator.Personnage;

public class Bouclier extends DecoracteurPersonnage {

   public Bouclier(Personnage p) {
      super(p);
   }

   @Override
   public String getNom() {
      return personnage.getNom() + " (+bouclier)";
   }

   @Override
   public int getDefense() {
      return personnage.getDefense() + 20;
   }
}
