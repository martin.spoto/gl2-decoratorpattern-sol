package decorator;

public abstract class DecoracteurPersonnage extends Personnage{
   protected Personnage personnage;

   protected DecoracteurPersonnage(Personnage p) {
      this.personnage = p;
   }

   @Override
   public String getNom() {
      return personnage.getNom();
   }

   @Override
   public int getAttaque() {
      return personnage.getAttaque();
   }

   @Override
   public int getDefense() {
      return personnage.getDefense();
   }

   @Override
   public void action() {
      personnage.action();
   }
}
